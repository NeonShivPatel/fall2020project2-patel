package spellingbee.client;

//import javafx.application.*;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.stage.*;
//import javafx.scene.*;
//import javafx.scene.paint.*;
//import javafx.scene.control.*;
//import javafx.scene.layout.*;
import javafx.event.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import spellingbee.network.Client;

public class GameTab extends Tab{
	private Client clientObject;
	
	public GameTab(Client clientele) {
		super("GameTab");
		this.clientObject = clientele;
		createGameTab();
	}
	
	public void createGameTab() {
		VBox vertical = new VBox();
		HBox lettersBtns = new HBox();
		TextField input = new TextField();
		input.setPrefWidth(200);
		
		//The Display row in the GUI
		HBox display = new HBox();
		TextField message = new TextField();
		TextField score = new TextField("0");
		message.setPrefWidth(350);
		score.setPrefWidth(60);
		
		//Loop to create Letter Buttons
		String letters = this.clientObject.sendAndWaitMessage("getAllLetters");
		for (int i=0; i < letters.length();  i++) {
			String singleLetter = Character.toString(letters.charAt(i));
			message.setText(message.getText()+singleLetter);
			Button letter = createLetterButton(singleLetter, input);
			lettersBtns.getChildren().add(letter);
		}//end loop
		
		
		//Creating Submit button
		Button submit = new Button("Submit");
		submit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				submitHandler(input, message, score);
			}
		});
		
		//Adding everything together
		display.getChildren().addAll(message, score);
		vertical.getChildren().addAll(lettersBtns, input, submit, display);
		this.setContent(vertical);
	}
	
	/**
	 * 
	 * @param input, TextField that represents the field in which the client inputs his word
	 * @param message, TextField that represents the field in which a message is displayed after every input
	 * @param score, TextField that represents the field in which the score is updated
	 */
	public void submitHandler(TextField input, TextField message, TextField score) {
		String response = this.clientObject.sendAndWaitMessage("wordCheck:" + input.getText());
		message.setText(response);
		this.clientObject.sendAndWaitMessage("addScore:" + input.getText());
		score.setText(this.clientObject.sendAndWaitMessage("getScore"));
	}
	
	
	/**
	 * Creates a button for a specific letter
	 * @param singleLetter, String that represents the letter the button is supposed to be
	 * @param input, TextField that represents the field in which the client inputs his word
	 * @return Button, A button that represents the letter given
	 */
	public Button createLetterButton(String singleLetter, TextField input) {
		if (singleLetter.equals(this.clientObject.sendAndWaitMessage("getCenterLetter"))) {
			Button letter = new Button(singleLetter);
			letter.setStyle("-fx-text-fill:red;");
			letter.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e) {
					input.setText(input.getText()+ singleLetter);
				}
			});
			return letter;
		}
		else {
			Button letter = new Button(singleLetter);
			letter.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e) {
					input.setText(input.getText()+ singleLetter);
				}
			});
			return letter;
		}
	}
}

package spellingbee.client;


import javafx.application.*;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
import javafx.stage.*;
import spellingbee.network.Client;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
//import javafx.scene.layout.*;

public class SpellingBeeClient extends Application{
	private Client clientele = new Client();
	
	@Override
	public void start(Stage arg0) throws Exception {
		Group root = new Group(); 
		TabPane tabPane = new TabPane();
		Tab gameTab = new GameTab(this.clientele);
		tabPane.getTabs().add(gameTab);
		root.getChildren().add(tabPane);
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		arg0.setTitle("Spelling Bee Game!"); 
		arg0.setScene(scene); 
		
		arg0.show(); 
	}

	public static void main(String[] args) {
        Application.launch(args);
    }
}

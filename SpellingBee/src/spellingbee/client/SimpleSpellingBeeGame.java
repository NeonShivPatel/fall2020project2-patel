package spellingbee.client;

public class SimpleSpellingBeeGame implements ISpellingBeeGame {
	private String letters;
	private char centerLetter;
	private int score;
	private final String[] solutions = new String[] {"traitor", "arid", "trio", "ratio", "arctic", "cardiac", "carotid"};
	
	//Constructor
	public SimpleSpellingBeeGame() {
		this.letters = "rtaidco";
		this.centerLetter = letters.charAt(3);
		this.score = 0;
	}
	
	/**
	 * Checks how many points the word inputted is worth
	 * @param String attempt, string that represents the word
	 * @return int points, the amount of points the word was worth
	 */
	public int getPointsForWord(String attempt) {
		int points;
		if (attempt.length() == 4) {
			points = 1;
		}
		else if (attempt.length() == 5 || attempt.length() == 6) {
			points = attempt.length();
		}
		else if (attempt.length() >= 7) {
			points = attempt.length() + 7;
		}
		else {
			System.out.println("Invalid word");
			points = 0;
		}
		return points;
	}

	/**
	 * Checks if the word given respects all the rules or not, and responds accordingly
	 * @param String attempt, string that represents the word
	 * @return String message, string that contains a message detailing if it is a good word or not, and why
	 */
	public String getMessage(String attempt) {
		String message = "";
		
		if (attempt.contains(Character.toString(this.centerLetter)) && validityChecker(attempt) && realWordChecker(attempt)) {
			message = "Good Job! This is a good word";
		}
		else if (!(attempt.contains(Character.toString(this.centerLetter)))){
			message = "This is a bad word. It does not contain the center letter!";
		}
		else if (validityChecker(attempt) == false) {
			message = "This is a bad word. Only use the letters provided!";
		}
		else if (realWordChecker(attempt) == false) {
			message = "This is a bad word. This is not an actual word";
		}
		return message;
	}
	
	/**
	 * Adds to the score field the amount of points the word is worth
	 * @param String attempt, string that represents the word
	 */
	public void addScore(String attempt) {
		this.score += getPointsForWord(attempt);
	}

	//Getter Methods
	public String getAllLetters() {
		return this.letters;
	}
	public char getCenterLetter() {
		return this.centerLetter;
	}
	public int getScore() {
		return this.score;
	}
	
	/**
	 * Checks if the letters utilised in the word inputted are part of the options or not
	 * @param String attempt, string that represents the word
	 * @return boolean validity, boolean that represents if the word uses the correct letter or not
	 */
	public boolean validityChecker(String attempt) {
		boolean validity = true;
		for (int i = 0; i < attempt.length(); i++) {
			int response = this.letters.indexOf(attempt.charAt(i));
			if (response == -1) {
				validity = false;
			}
		}
		return validity;
	}
	
	/**
	 * Checks if the word is an actual word that exists
	 * @param String attempt, string that represents the word
	 * @return boolean validity, boolean that represents if the word is real or not
	 */
	public boolean realWordChecker(String attempt) {
		boolean validity = true;
		for (int i = 0; i < solutions.length; i++) {
			if (!solutions[i].equalsIgnoreCase(attempt)){
				validity = false;
			}
			else {
				validity = true;
				return validity;
			}
		}
		return validity;
	}

}

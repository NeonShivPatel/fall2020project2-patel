package spellingbee.network;
import spellingbee.client.ISpellingBeeGame;
import spellingbee.client.SimpleSpellingBeeGame;
//import spellingbee.server.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	private ISpellingBeeGame spellingBee = new SimpleSpellingBeeGame();
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {
		if (inputLine.equals("getCenterLetter")) {
			return Character.toString(spellingBee.getCenterLetter());
		}
		
		else if (inputLine.equals("getAllLetters")) {
			return spellingBee.getAllLetters();
		}
		
		else if (inputLine.contains("wordCheck:")) {
			String[] word = inputLine.split("wordCheck:");
			String message = spellingBee.getMessage(word[1]);
			if (message.contains("good")) {
				return spellingBee.getMessage(word[1]) + ":" + spellingBee.getPointsForWord(word[1]);
			}
			else {
				return spellingBee.getMessage(word[1]) + ":" + "0";
			}
		}
		
		else if (inputLine.contains("getScore")) {
			return String.valueOf(spellingBee.getScore());
		}
		
		else if (inputLine.contains("addScore:")) {
			String[] word = inputLine.split("addScore:");
			String message = spellingBee.getMessage(word[1]);
			if (message.contains("good")) {
				spellingBee.addScore(word[1]);
			}
		}
		
		return null;
	}
}
